<?php

namespace Pondit\Calculator\AreaCalculator;


class Rectangle
{
    public $width ;
    public $length ;

    public function getArea()
    {
        return $this->width * $this->length;
    }
}