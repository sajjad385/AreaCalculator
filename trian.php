<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/21/2018
 * Time: 1:51 PM
 */

include_once "vendor/autoload.php";

use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Displayer;

$triangle1 = new Triangle();
$triangle1->base = 20;
$triangle1->height = 32;


$displayer=new Displayer();
$displayer->displaypre($triangle1->triangleArea());
$displayer->displayH1($triangle1->triangleArea());
$displayer->displaysimple($triangle1->triangleArea());